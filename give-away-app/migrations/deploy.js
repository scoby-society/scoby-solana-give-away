"use strict";
// Migrations are an early feature. Currently, they're nothing more than this
// single deploy script that's invoked from the CLI, injecting a provider
// configured from the workspace's Anchor.toml.
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
//import { Program } from "@project-serum/anchor";
//import { publicKey } from "@project-serum/anchor/dist/cjs/utils";
var spl_token_1 = require("@solana/spl-token");
var web3_js_1 = require("@solana/web3.js");
var anchor = require("@project-serum/anchor");
var splToken = require("@solana/spl-token");
// Configure the local cluster.
anchor.setProvider(anchor.AnchorProvider.local());
var idl = JSON.parse(require("fs").readFileSync("./target/idl/give_away_app.json", "utf8"));
//connection
var con = new web3_js_1.Connection("http://localhost:8899");
// Address of the deployed program.
var programId = new anchor.web3.PublicKey("C9Dybpn7t2Fr2p4vBNt8D6gd2Fn1twu2vrxSqSoEkVFd");
// Generate the program client from IDL.
var program = new anchor.Program(idl, programId);
// program owner wallet
var owner_fromWallet = anchor.web3.Keypair.generate();
module.exports = function (provider) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            // Configure client to use the provider.
            anchor.setProvider(anchor.AnchorProvider.local());
            return [2 /*return*/];
        });
    });
};
function Initialise(con, owner_fromWallet, programId) {
    return __awaiter(this, void 0, void 0, function () {
        var token_airdrop, _a, pda_admin, bump_admin;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, con.requestAirdrop(owner_fromWallet.publicKey, 100000000000)];
                case 1:
                    token_airdrop = _b.sent();
                    return [4 /*yield*/, con.confirmTransaction(token_airdrop)];
                case 2:
                    _b.sent();
                    return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("admin")], programId)];
                case 3:
                    _a = _b.sent(), pda_admin = _a[0], bump_admin = _a[1];
                    return [4 /*yield*/, program.rpc.initialize(bump_admin, {
                            accounts: {
                                giveAwayAcct: pda_admin,
                                owner: owner_fromWallet.publicKey,
                                systemProgram: anchor.web3.SystemProgram.programId
                            },
                            signers: [owner_fromWallet]
                        })];
                case 4:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function send_nativesol_pda(con, userWallet, programId, amount) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, user_pda, user_bump;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    console.log("send sol to pda process started..");
                    return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("usersol")], programId)];
                case 1:
                    _a = _b.sent(), user_pda = _a[0], user_bump = _a[1];
                    console.log("rpc call for transfer sol to PDA started");
                    return [4 /*yield*/, program.rpc.sendSolPda(user_bump, new anchor.BN(amount * web3_js_1.LAMPORTS_PER_SOL), {
                            accounts: {
                                userpdaAcct: user_pda,
                                sender: userWallet.publicKey,
                                systemProgram: anchor.web3.SystemProgram.programId
                            },
                            signers: [userWallet]
                        })];
                case 2:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function send_spltoken_pda(con, userWallet, programId, amount) {
    return __awaiter(this, void 0, void 0, function () {
        var mint, myToken_acct, _a, UserStats, user_state_bump, _b, user_token_pda, user_token_bump, mintkey;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    console.log("token transfer to PDA from user started..");
                    return [4 /*yield*/, (0, spl_token_1.createMint)(con, userWallet, userWallet.publicKey, null, 0)];
                case 1:
                    mint = _c.sent();
                    console.log("mint key =", mint);
                    return [4 /*yield*/, (0, spl_token_1.getOrCreateAssociatedTokenAccount)(con, userWallet, mint, userWallet.publicKey)];
                case 2:
                    myToken_acct = _c.sent();
                    console.log("sender wallet key:", userWallet.publicKey.toString());
                    //minting 100 new tokens to the token address we just created
                    return [4 /*yield*/, (0, spl_token_1.mintTo)(con, userWallet, mint, myToken_acct.address, userWallet.publicKey, 100)];
                case 3:
                    //minting 100 new tokens to the token address we just created
                    _c.sent();
                    return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("user-stats")], programId)];
                case 4:
                    _a = _c.sent(), UserStats = _a[0], user_state_bump = _a[1];
                    return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("seedtoken")], programId)];
                case 5:
                    _b = _c.sent(), user_token_pda = _b[0], user_token_bump = _b[1];
                    mintkey = mint.toString();
                    console.log(mintkey);
                    console.log("rpc call for token transfer from user to PDA started..");
                    return [4 /*yield*/, program.rpc.sendSpltokenPda(user_token_bump, new anchor.BN(amount), mintkey, {
                            accounts: {
                                userStats: UserStats,
                                userTokenPda: user_token_pda,
                                mint: mint,
                                creator: userWallet.publicKey,
                                initializerDepositTokenAccount: myToken_acct.address,
                                systemProgram: anchor.web3.SystemProgram.programId,
                                rent: web3_js_1.SYSVAR_RENT_PUBKEY,
                                tokenProgram: spl_token_1.TOKEN_PROGRAM_ID
                            },
                            signers: [userWallet]
                        })];
                case 6:
                    _c.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function send_spltoken_winner(con, winnerWallet, mint_address, authority, user_token_pda, user_token_bump, programId, amount) {
    return __awaiter(this, void 0, void 0, function () {
        var myToken_acct;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    console.log("token from PDA to winner starrted");
                    return [4 /*yield*/, (0, spl_token_1.getOrCreateAssociatedTokenAccount)(con, winnerWallet, mint_address, winnerWallet.publicKey)];
                case 1:
                    myToken_acct = _a.sent();
                    console.log(myToken_acct);
                    // const [user_token_pda, user_token_bump] = await anchor.web3.PublicKey.findProgramAddress(
                    //   [Buffer.from("seedtoken")],
                    //   programId
                    // );
                    console.log("rpc call for token transfer from PDA to winner");
                    return [4 /*yield*/, program.rpc.sendSpltokenWinner(user_token_bump, new anchor.BN(amount * web3_js_1.LAMPORTS_PER_SOL), {
                            accounts: {
                                userTokenPda: user_token_pda,
                                winnerAssociatedTokenAccount: myToken_acct.address,
                                creator: authority,
                                systemProgram: anchor.web3.SystemProgram.programId,
                                tokenProgram: splToken.TOKEN_PROGRAM_ID
                            },
                            signers: []
                        })];
                case 2:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
function send_nativesol_winner(con, winnerWallet, programId, amount) {
    return __awaiter(this, void 0, void 0, function () {
        var _a, user_pda, user_bump;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("usersol")], programId)];
                case 1:
                    _a = _b.sent(), user_pda = _a[0], user_bump = _a[1];
                    console.log("rpc call for sol transfer from PDA to winner");
                    return [4 /*yield*/, program.rpc.sendNativesolWinner(user_bump, new anchor.BN(amount * web3_js_1.LAMPORTS_PER_SOL), {
                            accounts: {
                                userPda: user_pda,
                                winner: winnerWallet.publicKey,
                                systemProgram: anchor.web3.SystemProgram.programId
                            },
                            signers: []
                        })];
                case 2:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    });
}
console.log("start..");
function call_initialise() {
    Initialise(con, owner_fromWallet, programId).then(function () { return console.log("Success"); });
}
function call_send_nativesol_pda() {
    return __awaiter(this, void 0, void 0, function () {
        var amount, userWallet, userWallet_airdrop;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    amount = 1;
                    userWallet = anchor.web3.Keypair.generate();
                    return [4 /*yield*/, con.requestAirdrop(userWallet.publicKey, 100000000000)];
                case 1:
                    userWallet_airdrop = _a.sent();
                    return [4 /*yield*/, con.confirmTransaction(userWallet_airdrop)];
                case 2:
                    _a.sent();
                    send_nativesol_pda(con, userWallet, programId, amount).then(function () { return console.log("Success"); });
                    console.log("send to PDA is completed");
                    return [2 /*return*/];
            }
        });
    });
}
function call_send_spltoken_pda() {
    return __awaiter(this, void 0, void 0, function () {
        var amount, userWallet, userWallet_airdrop;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    amount = 1;
                    userWallet = anchor.web3.Keypair.generate();
                    return [4 /*yield*/, con.requestAirdrop(userWallet.publicKey, 900000000000)];
                case 1:
                    userWallet_airdrop = _a.sent();
                    return [4 /*yield*/, con.confirmTransaction(userWallet_airdrop)];
                case 2:
                    _a.sent();
                    send_spltoken_pda(con, userWallet, programId, amount).then(function () { return console.log("send_spltoken_pda Success"); });
                    return [2 /*return*/];
            }
        });
    });
}
function call_send_spltoken_winner() {
    return __awaiter(this, void 0, void 0, function () {
        var amount, winnerWallet, winnerWallet_airdrop, _a, UserStatsac, user_state_bump, state, mint_address, authority, user_token_pda, user_token_bump, userWallet_airdrop, pda_bal, authority_bal, winner_bal;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    console.log("here we go");
                    amount = 1;
                    winnerWallet = anchor.web3.Keypair.generate();
                    return [4 /*yield*/, con.requestAirdrop(winnerWallet.publicKey, 100000000000)];
                case 1:
                    winnerWallet_airdrop = _b.sent();
                    return [4 /*yield*/, con.confirmTransaction(winnerWallet_airdrop)];
                case 2:
                    _b.sent();
                    console.log("winner wallet pubkey", winnerWallet.publicKey.toString());
                    return [4 /*yield*/, anchor.web3.PublicKey.findProgramAddress([Buffer.from("user-stats")], programId)];
                case 3:
                    _a = _b.sent(), UserStatsac = _a[0], user_state_bump = _a[1];
                    return [4 /*yield*/, program.account.userStats.fetch(UserStatsac)];
                case 4:
                    state = _b.sent();
                    console.log(state.tokenpda, state.mintkey, state.authority);
                    mint_address = new web3_js_1.PublicKey(state.mintkey);
                    authority = state.authority;
                    user_token_pda = state.tokenpda;
                    user_token_bump = state.bump;
                    console.log("mint_address=", mint_address);
                    console.log("authority", authority.toString());
                    console.log("user_token_pda", user_token_pda.toString());
                    console.log("user_token_bump", user_token_bump);
                    return [4 /*yield*/, con.requestAirdrop(user_token_pda, 900000000000)];
                case 5:
                    userWallet_airdrop = _b.sent();
                    return [4 /*yield*/, con.confirmTransaction(userWallet_airdrop)];
                case 6:
                    _b.sent();
                    return [4 /*yield*/, con.getBalance(user_token_pda)];
                case 7:
                    pda_bal = _b.sent();
                    return [4 /*yield*/, con.getBalance(authority)];
                case 8:
                    authority_bal = _b.sent();
                    return [4 /*yield*/, con.getBalance(winnerWallet.publicKey)];
                case 9:
                    winner_bal = _b.sent();
                    console.log("pda_bal=", pda_bal);
                    console.log("authority_bal", authority_bal);
                    console.log("winner bal", winner_bal);
                    //process.exit();
                    send_spltoken_winner(con, winnerWallet, mint_address, authority, user_token_pda, user_token_bump, programId, amount).then(function () { return console.log("send_spltoken_pda Success"); });
                    return [2 /*return*/];
            }
        });
    });
}
function call_send_nativesol_winner() {
    return __awaiter(this, void 0, void 0, function () {
        var amount, winnerWallet;
        return __generator(this, function (_a) {
            amount = 1;
            winnerWallet = anchor.web3.Keypair.generate();
            send_nativesol_winner(con, winnerWallet, programId, amount).then(function () { return console.log("send_nativesol_winner Success"); });
            return [2 /*return*/];
        });
    });
}
//call_initialise()
//call_send_nativesol_pda()
//call_send_spltoken_pda()
//call_send_spltoken_winner()
//call_send_nativesol_winner()
