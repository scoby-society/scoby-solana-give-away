// Migrations are an early feature. Currently, they're nothing more than this
// single deploy script that's invoked from the CLI, injecting a provider
// configured from the workspace's Anchor.toml.

//import { Program } from "@project-serum/anchor";
//import { publicKey } from "@project-serum/anchor/dist/cjs/utils";

import {AccountLayout, TOKEN_PROGRAM_ID,NATIVE_MINT, getAccount,createMint,getOrCreateAssociatedTokenAccount,mintTo, getMinimumBalanceForRentExemptMultisig, getMinimumBalanceForRentExemptMint } from "@solana/spl-token";
import { clusterApiUrl, Connection, Keypair, LAMPORTS_PER_SOL,PublicKey, Transaction,SYSVAR_RENT_PUBKEY, MAX_SEED_LENGTH } from '@solana/web3.js';
const anchor = require("@project-serum/anchor");
const splToken = require("@solana/spl-token");
// Configure the local cluster.
anchor.setProvider(anchor.AnchorProvider.local());
const idl = JSON.parse(
  require("fs").readFileSync("./target/idl/give_away_app.json", "utf8")
);
//connection
const con = new Connection("http://localhost:8899");

// Address of the deployed program.
const programId = new anchor.web3.PublicKey("C9Dybpn7t2Fr2p4vBNt8D6gd2Fn1twu2vrxSqSoEkVFd");

// Generate the program client from IDL.
const program = new anchor.Program(idl, programId);

// program owner wallet

const owner_fromWallet = anchor.web3.Keypair.generate();

module.exports = async function (provider) {
  // Configure client to use the provider.
  anchor.setProvider(anchor.AnchorProvider.local());

  // Add your deploy script here.
};



async function Initialise(con:Connection,owner_fromWallet:Keypair,programId) {

  // airdrop token to wallet

  let token_airdrop= await con.requestAirdrop(owner_fromWallet.publicKey, 100000000000);
  await con.confirmTransaction(token_airdrop);

  const [pda_admin, bump_admin] = await anchor.web3.PublicKey.findProgramAddress(
    [Buffer.from("admin")],
    programId
  );



  await program.rpc.initialize(bump_admin, {
    accounts: {
      giveAwayAcct: pda_admin,
      owner: owner_fromWallet.publicKey,
      systemProgram: anchor.web3.SystemProgram.programId,

    },
    signers: [owner_fromWallet]
  });

}

async function send_nativesol_pda(con:Connection,
  userWallet:Keypair,
  programId,amount:number) {

    console.log("send sol to pda process started..");

    const [user_pda, user_bump] = await anchor.web3.PublicKey.findProgramAddress(
      [Buffer.from("usersol")],
      programId
    );
    console.log("rpc call for transfer sol to PDA started");
    await program.rpc.sendSolPda(user_bump,new anchor.BN(amount*LAMPORTS_PER_SOL), {
      accounts: {
        userpdaAcct: user_pda,
        sender: userWallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
          },
      signers: [userWallet]
    }); 
}

async function send_spltoken_pda(con:Connection,
  userWallet:Keypair,
  programId,amount:number) {

    console.log("token transfer to PDA from user started..");
    // create mint
    let mint = await createMint(con, userWallet, userWallet.publicKey, null, 0);
    console.log("mint key =",mint);
    
      //get the token accont of this solana address, if it does not exist, create it
    var myToken_acct = await getOrCreateAssociatedTokenAccount(con,
           userWallet,
           mint,
           userWallet.publicKey);
    console.log("sender wallet key:",userWallet.publicKey.toString());
    //minting 100 new tokens to the token address we just created
    await mintTo(con,userWallet,mint,
      myToken_acct.address, userWallet.publicKey, 100);

    // create token PDA

    const [UserStats, user_state_bump] = await anchor.web3.PublicKey.findProgramAddress(
      [Buffer.from("user-stats")],
      programId
    );

    const [user_token_pda, user_token_bump] = await anchor.web3.PublicKey.findProgramAddress(
      [Buffer.from("seedtoken")],
      programId
    );

   let mintkey=mint.toString();
   console.log(mintkey);
   console.log("rpc call for token transfer from user to PDA started..");
    await program.rpc.sendSpltokenPda(user_token_bump,new anchor.BN(amount),mintkey, {
      accounts: {
        userStats: UserStats,
        userTokenPda: user_token_pda,
        mint: mint,
        creator: userWallet.publicKey,
        initializerDepositTokenAccount: myToken_acct.address,
        systemProgram: anchor.web3.SystemProgram.programId,
        rent: SYSVAR_RENT_PUBKEY,
        tokenProgram:TOKEN_PROGRAM_ID
      },
      signers: [userWallet]
    });

  
}

async function send_spltoken_winner(con:Connection,
  winnerWallet:Keypair,mint_address:PublicKey,authority:PublicKey,user_token_pda,user_token_bump,
  programId,amount:number) {

      console.log("token from PDA to winner starrted");
   //  Associated token  account for winner
      var myToken_acct = await getOrCreateAssociatedTokenAccount(con,
        winnerWallet,
        mint_address,
        winnerWallet.publicKey);
 



      console.log(myToken_acct);
      

        // const [user_token_pda, user_token_bump] = await anchor.web3.PublicKey.findProgramAddress(
        //   [Buffer.from("seedtoken")],
        //   programId
        // );
        console.log("rpc call for token transfer from PDA to winner");
        await program.rpc.sendSpltokenWinner(
          user_token_bump,new anchor.BN(amount*LAMPORTS_PER_SOL), {
          accounts: {
            userTokenPda: user_token_pda,
            winnerAssociatedTokenAccount: myToken_acct.address,
            creator: authority,
            systemProgram: anchor.web3.SystemProgram.programId,
            tokenProgram:splToken.TOKEN_PROGRAM_ID
          },
          signers: []
        });



  
}

async function send_nativesol_winner(con:Connection,
  winnerWallet:Keypair,
  programId,amount:number) {

    const [user_pda, user_bump] = await anchor.web3.PublicKey.findProgramAddress(
      [Buffer.from("usersol")],
      programId
    );
    console.log("rpc call for sol transfer from PDA to winner");
    await program.rpc.sendNativesolWinner(user_bump,new anchor.BN(amount*LAMPORTS_PER_SOL), {
      accounts: {
        userPda:user_pda,
        winner: winnerWallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
       
      },
      signers: []
    });



  
}

console.log("start..")

function call_initialise() {

Initialise(con,owner_fromWallet,programId).then(() => console.log("Success"));

}

async function call_send_nativesol_pda(){

  let amount =1;
  let userWallet=anchor.web3.Keypair.generate();
  // airdrop token to wallet
  let userWallet_airdrop= await con.requestAirdrop(userWallet.publicKey, 100000000000);
  await con.confirmTransaction(userWallet_airdrop);
  send_nativesol_pda(con,userWallet,programId,amount).then(() => console.log("Success"));
  console.log("send to PDA is completed");
}

async function call_send_spltoken_pda() {
  
  let amount=1;
  let userWallet=anchor.web3.Keypair.generate();
  let userWallet_airdrop= await con.requestAirdrop(userWallet.publicKey, 900000000000);
  await con.confirmTransaction(userWallet_airdrop);
  send_spltoken_pda(con,userWallet,programId,amount).then(() => console.log("send_spltoken_pda Success"));
  
}

async function call_send_spltoken_winner() {
  console.log("here we go");
  let amount=1;

  const winnerWallet=anchor.web3.Keypair.generate();
  let winnerWallet_airdrop= await con.requestAirdrop(winnerWallet.publicKey, 100000000000);
  await con.confirmTransaction(winnerWallet_airdrop);
 

  console.log("winner wallet pubkey",winnerWallet.publicKey.toString());
   const [UserStatsac, user_state_bump] = await anchor.web3.PublicKey.findProgramAddress(
    [Buffer.from("user-stats")],
    programId
  );

  const state=await program.account.userStats.fetch(UserStatsac);
  console.log(state.tokenpda,state.mintkey,state.authority);
  let mint_address= new PublicKey(state.mintkey);
  let authority=state.authority;
  let user_token_pda=state.tokenpda;
  let user_token_bump=state.bump;
  console.log("mint_address=",mint_address);
  console.log("authority",authority.toString());
  console.log("user_token_pda",user_token_pda.toString());
  console.log("user_token_bump",user_token_bump);
  let userWallet_airdrop= await con.requestAirdrop(user_token_pda, 900000000000);
  await con.confirmTransaction(userWallet_airdrop);
  let pda_bal=await con.getBalance(user_token_pda);
  let authority_bal=await con.getBalance(authority);
  let winner_bal=await con.getBalance(winnerWallet.publicKey);
  console.log("pda_bal=",pda_bal);
  console.log("authority_bal",authority_bal);
  console.log("winner bal",winner_bal);
  //process.exit();
  send_spltoken_winner(con,winnerWallet,mint_address,authority,user_token_pda,user_token_bump,programId,amount).then(() => console.log("send_spltoken_pda Success"));

}

async function call_send_nativesol_winner() {
  let amount=1;
  let winnerWallet=anchor.web3.Keypair.generate();
 send_nativesol_winner(con,winnerWallet,programId,amount).then(() => console.log("send_nativesol_winner Success"));

}
//call_initialise()
//call_send_nativesol_pda()

//call_send_spltoken_pda()

//call_send_spltoken_winner()

//call_send_nativesol_winner()
