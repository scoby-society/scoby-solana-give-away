use anchor_lang::{prelude::*};
use anchor_spl::{token::{TokenAccount, Mint, Token}, associated_token::AssociatedToken};
use anchor_spl::token::Transfer;

declare_id!("C9Dybpn7t2Fr2p4vBNt8D6gd2Fn1twu2vrxSqSoEkVFd");

#[program]
pub mod give_away_app {
    use std::process::exit;

    use super::*;

    pub fn initialize_owner(ctx: Context<Initializeowner>,_user_bump:u32) -> Result<()> {
        let accountuser = &mut (ctx.accounts.account_user);
        accountuser.owner_id=ctx.accounts.owner.key();
        accountuser.platform_id=ctx.accounts.platform.key();
        msg!("completed {}",accountuser.owner_id);
        Ok(())
    }

    pub fn initializepdasol(ctx: Context<Initializepdasol>,_bump:u8,_msg_id: String) -> Result<()> {
        msg!("account initialised for Sol transfer : {}",ctx.accounts.sender.key());        
        
        Ok(())

    }

    pub fn initializestatepda(_ctx: Context<Initialisedstatepda>,_bump:u8,_msg_id: String) -> Result<()> {

        msg!("state got Initialised");
        
            Ok(())
    }

    pub fn initialisetokenpda(ctx: Context<Initialisetokenpda>,_bump1:u8,_msg_id: String) -> Result<()> {

        msg!("token got Initialised");
        let pda = ctx.accounts.tokenpda.key();
        msg!("token pda : {}",pda);
        Ok(())
    }

    pub fn sendsolpda(ctx: Context<Sendsolpda>,_bump: u8,amount:u64,msg_id:String) -> Result<()> {
        msg!("send sol start on backend");
     let userpda= &mut(ctx.accounts.user_pda_acct);
     
     
     let sender=ctx.accounts.sender.key();
     let bump_sol_vector=_bump.to_le_bytes();
     let inner=vec![sender.as_ref(),msg_id.as_ref(),bump_sol_vector.as_ref()];
     let outer_sol=vec![inner.as_slice()];
     
     let ix = anchor_lang::solana_program::system_instruction::transfer(
         &ctx.accounts.sender.key(),
         &userpda.key(),
         amount,
     );

     anchor_lang::solana_program::program::invoke_signed(
         &ix,
         &[ctx.accounts.sender.to_account_info(),userpda.to_account_info()],
         outer_sol.as_slice())?;

         
     userpda.amount=amount;

     msg!("transfered successfully to PDA sol");   
      
     Ok(())

        }

        pub fn sendtokenpda(ctx: Context<SendTokenPDA>,_bump1:u8,_bump2:u8,_amount:u64,_msg_id: String) -> Result<()> {
            msg!("token process start for PDA transfer...");
            let state = &mut(ctx.accounts.statepda);
            msg!("before: {}",state.amount);
            msg!("{} bump after",state.bump);
            state.bump=_bump1;
            state.amount=_amount;
            msg!("after: {}",state.amount);
            msg!("{} bump after",state.bump);
            let bump_vector=_bump1.to_le_bytes();
            let dep=&mut ctx.accounts.deposit_token_account.key();
            let sender=&ctx.accounts.owner;
            let inner=vec![sender.key.as_ref(),dep.as_ref(),_msg_id.as_ref(),"state".as_ref(),bump_vector.as_ref()];
            let outer=vec![inner.as_slice()];
         
         
            let transfer_instruction = Transfer { 
                
                from : ctx.accounts.deposit_token_account.to_account_info(),
                to : ctx.accounts.tokenpda.to_account_info(),
                authority: sender.to_account_info()
            
            };

            let cpi_ctx = CpiContext::new_with_signer(
             ctx.accounts.token_program.to_account_info(),
             transfer_instruction,
             outer.as_slice(),
         );
          
           msg!("transfer call start");
     
            anchor_spl::token::transfer(cpi_ctx, _amount)?;
            ctx.accounts.tokenpda.reload()?;
            msg!("token pda key {}",ctx.accounts.tokenpda.key());
            msg!("token after transfer to reciever in PDA {}",ctx.accounts.tokenpda.amount);
     
            msg!("succesfully transfered");
     
             Ok(())
     
         }
     
         pub fn claimsol(ctx: Context<ClaimSol>,_bump:u8,_msg_id:String) -> Result<()> {
            msg!("claim sol from PDA start from backend...");
            let ownerpda= &ctx.accounts.owner;
            let platform_key_provided=ctx.accounts.platform.key();
            let platform_key=ownerpda.platform_id;
            msg!("platform_key_provided:{}",platform_key_provided);
            msg!("platform_key: {}",platform_key);

            if platform_key_provided!=platform_key 
            {
                  msg!("platform ID not match");
                  exit(0);
            }
            let userpda= &mut(ctx.accounts.user_pda_acct);
            let bump_sol_vector=_bump.to_le_bytes();
            let sender= ctx.accounts.sender.key();
            let inner=vec![sender.as_ref(),_msg_id.as_ref(),bump_sol_vector.as_ref()];
            let _outer_sol=vec![inner.as_slice()];
            let amount=userpda.amount;
            
   
          **ctx.accounts.user_pda_acct.to_account_info().try_borrow_mut_lamports()? -= amount;
          **ctx.accounts.winner.try_borrow_mut_lamports()? += amount;
           msg!("reciever after transfer done");
         
           msg!("process end");
           
           Ok(())
       }
   
       pub fn initialisewinner(_ctx: Context<InitialiseTokenWinner>,_bump1:u8,_bump2:u8,_amount:u64,_msg_id: String) -> Result<()>
       {
           msg!("winner associted token account initialise succesfullyy...");
           Ok(())
       }

       pub fn sendtokenwinner(ctx: Context<SendTokenWinner>,_bump1:u8,_bump2:u8,_amount:u64,_msg_id: String) -> Result<()>
        {
        msg!("token transfer to winner started from backend...");

       let bump_vector=_bump1.to_le_bytes();
       let dep=&mut ctx.accounts.deposit_token_account.key();
       let sender=&ctx.accounts.sender;
       let inner=vec![sender.key.as_ref(),dep.as_ref(),_msg_id.as_ref(),"state".as_ref(),bump_vector.as_ref()];
       let outer=vec![inner.as_slice()];

       let ownerpda= &ctx.accounts.owner;
       let platform_key_provided=ctx.accounts.platform.key();
       let platform_key=ownerpda.platform_id;
       msg!("platform_key_provided:{}",platform_key_provided);
       msg!("platform_key: {}",platform_key);

       if platform_key_provided!=platform_key 
            {
                  msg!("platform ID not match");
                  exit(0);
            }

       let transfer_instruction = Transfer{
           from: ctx.accounts.tokenpda.to_account_info(),
           to:   ctx.accounts.wallet_to_deposit_to.to_account_info(),
           authority: ctx.accounts.statepda.to_account_info()
       };
       
       let cpi_ctx = CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        transfer_instruction,
        outer.as_slice(),
    );

      msg!("trasnfer call start");

       anchor_spl::token::transfer(cpi_ctx, _amount)?;


        Ok(())
    }

    pub fn cancel_sol(ctx: Context<CancelSol>,_bump:u8,_msg_id:String) -> Result<()> {
               
        msg!("cancel sol process started from backend..");
        let userpda= &mut(ctx.accounts.user_pda_acct);
        let bump_sol_vector=_bump.to_le_bytes();
        let sender= ctx.accounts.sender.key();
        let inner=vec![sender.as_ref(),bump_sol_vector.as_ref()];
        let _outer_sol=vec![inner.as_slice()];
        let amount=userpda.amount;
    
        let ownerpda= &ctx.accounts.ownerpda;
        let owner_key=ctx.accounts.owner.key();
        let owner_key_real=ownerpda.owner_id;
        msg!("owner_key for sol:{}",owner_key);
        msg!("owner_key_real for sol:{}",owner_key_real);
        if owner_key!=owner_key_real
        {
            msg!("owner key doesnt match");
            exit(0);
        }
        
      **ctx.accounts.user_pda_acct.to_account_info().try_borrow_mut_lamports()? -= amount;
      **ctx.accounts.sender.try_borrow_mut_lamports()? += amount;
        msg!("transaction cancel and SOL goes back to user");
       
       msg!("process end");

        
        Ok(())
    }

    pub fn initialise_sender_token(_ctx: Context<InitialiseSenderToken>,_bump1:u8,_bump2:u8,_amount:u64,_msg_id: String) -> Result<()>
    {
        msg!("initialise token account for sender..");
        Ok(())
    }

    pub fn canceltoken(ctx: Context<Canceltoken>,_bump1:u8,_bump2:u8,_amount:u64,_msg_id: String) -> Result<()>
    {

      msg!("cancel token start from backend");

       let bump_vector=_bump1.to_le_bytes();
       let dep=&mut ctx.accounts.deposit_token_account.key();
       let sender=&ctx.accounts.sender;
       let inner=vec![sender.key.as_ref(),dep.as_ref(),_msg_id.as_ref(),"state".as_ref(),bump_vector.as_ref()];
       let outer=vec![inner.as_slice()];

       let ownerpda= &ctx.accounts.ownerpda;
       let owner_key=ctx.accounts.owner.key();
       let owner_key_real=ownerpda.owner_id;
       msg!("owner_key:{}",owner_key);
       msg!("owner_key_real:{}",owner_key_real);
       if owner_key!=owner_key_real
       {
           msg!("owner key doesnt match");
           exit(0);
       }
    
    
       let transfer_instruction = Transfer{
           from: ctx.accounts.tokenpda.to_account_info(),
           to:   ctx.accounts.wallet_to_deposit_to.to_account_info(),
           authority: ctx.accounts.statepda.to_account_info()
       };
       
       let cpi_ctx = CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        transfer_instruction,
        outer.as_slice(),
    );

   
       msg!("trasnfer call start");

       anchor_spl::token::transfer(cpi_ctx, _amount)?;

 

        Ok(())
    }

   
}

#[derive(Accounts)]
pub struct Initializeowner<'info> {
    #[account(init,
              seeds = [owner.key.as_ref()],
              bump,
              payer = owner,
              space = 8+32+32)]
    pub account_user:Account<'info,AccountUser>,
    #[account(mut)]
    pub owner: Signer<'info>,
    ///CHECK no read write operation
    pub platform: AccountInfo<'info>,
    pub system_program: Program<'info,System>
}

#[derive(Accounts)]
#[instruction(_bump : u8 , _msg_id : String)]
pub struct Initializepdasol<'info> {

    #[account(
        init,
        payer = sender,
        seeds=[sender.key.as_ref(),_msg_id.as_ref()],
        bump ,
        space=100
    )]   
  
    pub user_pda_acct:Account<'info,UserPdaAcct>,
    #[account(mut)]  
    pub sender: Signer<'info>,  
    pub system_program: Program<'info,System>,

}

#[derive(Accounts)]
#[instruction(_bump : u8,_msg_id: String)]
pub struct Initialisedstatepda<'info> {
    #[account(
        init,
        payer = owner,
        seeds=[owner.key.as_ref(),deposit_token_account.key().as_ref(),_msg_id.as_ref(),"state".as_ref()],
        bump,
        space=200
    )]
    statepda: Account<'info, State>,
    #[account(mut)]
    pub owner: Signer<'info>,
    #[account(mut)]
    pub deposit_token_account: Account<'info, TokenAccount>,
    pub system_program: Program<'info,System>,

}

#[derive(Accounts)]
#[instruction(_bump : u8,_msg_id: String)]
pub struct Initialisetokenpda<'info> {

    #[account(
        init,
        seeds = [owner.key.as_ref(),deposit_token_account.key().as_ref(),_msg_id.as_ref()],
        bump,
        payer = owner,
        token::mint = mint,
        token::authority = statepda,
     )]
     
    pub tokenpda: Account<'info, TokenAccount>,
    pub statepda: Account<'info,State>,
    pub mint: Account<'info, Mint>,
    #[account(mut)]
    pub owner: Signer<'info>,
    #[account(mut)]
    pub deposit_token_account: Account<'info, TokenAccount>,
    pub system_program: Program<'info,System>,
    pub rent: Sysvar<'info, Rent>,
    pub token_program: Program<'info,Token>,
   
}

#[derive(Accounts)]

pub struct Sendsolpda<'info> {
    #[account(mut)]
    pub user_pda_acct:Account<'info,UserPdaAcct>,
    #[account(mut)]
    sender: Signer<'info>,
    pub system_program: Program<'info,System>,

}

#[derive(Accounts)]
pub struct SendTokenPDA<'info> {

    #[account(mut)]
    pub tokenpda: Account<'info, TokenAccount>,
    pub statepda: Account<'info,State>,
    pub mint: Account<'info, Mint>,
    #[account(mut)]
    pub owner: Signer<'info>,
    #[account(mut)]
    pub deposit_token_account: Account<'info, TokenAccount>,
    pub system_program: Program<'info,System>,
    pub rent: Sysvar<'info, Rent>,
    pub token_program: Program<'info,Token>,
   
}

#[derive(Accounts)]

pub struct ClaimSol<'info> {
    #[account(mut)]
    pub user_pda_acct:Account<'info,UserPdaAcct>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    pub sender: AccountInfo<'info>,
    /// CHECK: This is not dangerous because we don't read or write from this account
    #[account(mut)]
    pub winner: AccountInfo<'info>,
    pub platform: Signer<'info>,
    pub owner : Account<'info,AccountUser>,
    pub system_program: Program<'info,System>,


}

#[derive(Accounts)]
pub struct InitialiseTokenWinner<'info> {

    #[account(mut)]
    pub tokenpda: Account<'info, TokenAccount>,
    pub statepda: Account<'info,State>,
    #[account(
        init,
        payer = reciever,
        associated_token::mint = mint,
        associated_token::authority = reciever,
    )]
    pub wallet_to_deposit_to: Account<'info,TokenAccount>,
    /// CHECK not read write to this account
    pub sender : AccountInfo<'info>,
    #[account(mut)]
    pub reciever: Signer<'info>,    
    pub mint: Account<'info,Mint>,
    pub system_program: Program<'info,System>,
    pub rent: Sysvar<'info, Rent>,
    associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info,Token>,
}

#[derive(Accounts)]
pub struct SendTokenWinner<'info> {

     #[account(mut)]
     pub tokenpda: Account<'info, TokenAccount>,
     pub statepda: Account<'info,State>,
     #[account(mut)]
     pub wallet_to_deposit_to: Account<'info,TokenAccount>,
     /// CHECK not read write to this account
     pub sender : AccountInfo<'info>,
     pub deposit_token_account: Account<'info, TokenAccount>,
     #[account(mut)]
     /// CHECK not read write to this account
     pub reciever: AccountInfo<'info>, 
     pub platform: Signer<'info>, 
     pub owner : Account<'info,AccountUser>,
     //pub mint: Account<'info,Mint>,
     pub system_program: Program<'info,System>,
     pub rent: Sysvar<'info, Rent>,
     pub associated_token_program: Program<'info, AssociatedToken>,
     pub token_program: Program<'info,Token>,
}

#[derive(Accounts)]
pub struct CancelSol<'info> {
    #[account(mut)]
    pub user_pda_acct:Account<'info,UserPdaAcct>,
    ///CHECK no read or write
    #[account(mut)]
    pub sender: AccountInfo<'info>,
    pub owner: Signer<'info>,
    pub ownerpda : Account<'info,AccountUser>,
    pub system_program: Program<'info,System>,


}

#[derive(Accounts)]
pub struct InitialiseSenderToken<'info> {

    #[account(mut)]
    pub tokenpda: Account<'info, TokenAccount>,
    pub statepda: Account<'info,State>,
    #[account(
        init,
        payer = sender,
        associated_token::mint = mint,
        associated_token::authority = sender,
    )]
    pub wallet_to_deposit_to: Account<'info,TokenAccount>,
    #[account(mut)]
    pub sender : Signer<'info>,
  
    pub mint: Account<'info,Mint>,
    pub system_program: Program<'info,System>,
    pub rent: Sysvar<'info, Rent>,
    associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info,Token>,
}

#[derive(Accounts)]
pub struct Canceltoken<'info> {

    #[account(mut)]
    pub tokenpda: Account<'info, TokenAccount>,
    pub statepda: Account<'info,State>,
    #[account(mut)]
    pub wallet_to_deposit_to: Account<'info,TokenAccount>,
    #[account(mut)]
    /// CHECK not read write to this account
    pub sender : AccountInfo<'info>,
    pub owner : Signer<'info>,
    pub deposit_token_account: Account<'info, TokenAccount>,  
    pub ownerpda : Account<'info,AccountUser>,
    //pub mint: Account<'info,Mint>,
    pub system_program: Program<'info,System>,
    pub rent: Sysvar<'info, Rent>,
    associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info,Token>,
}

#[account]

pub struct AccountUser {
    pub owner_id: Pubkey,
    pub platform_id: Pubkey,
        }

#[account]
pub struct UserPdaAcct {
  pub amount: u64,
     }

     #[account]
     #[derive(Default)]
     pub struct State {
         bump: u8,
         amount: u64,           
}

