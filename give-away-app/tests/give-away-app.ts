import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { GiveAwayApp } from "../target/types/give_away_app";

import {
  AccountLayout,
  ASSOCIATED_TOKEN_PROGRAM_ID,
  TOKEN_PROGRAM_ID,NATIVE_MINT,
  createAssociatedTokenAccountInstruction,
  getAccount,createInitializeMintInstruction,
  createMint,getOrCreateAssociatedTokenAccount,
  mintTo, createAssociatedTokenAccount,
   getAssociatedTokenAddress, 
   transfer} from "@solana/spl-token";

import { sendAndConfirmTransaction } from "@solana/web3.js";


  const { 
    SystemProgram, PublicKey,
    Connection,Keypair,LAMPORTS_PER_SOL,
    Transaction,SYSVAR_RENT_PUBKEY,MAX_SEED_LENGTH } = anchor.web3;

describe("give_away_app", () => {
  // Configure the client to use the local cluster.
  anchor.setProvider(anchor.AnchorProvider.env());
  const program_id = new anchor.web3.PublicKey("C9Dybpn7t2Fr2p4vBNt8D6gd2Fn1twu2vrxSqSoEkVFd")
  const idl = JSON.parse(
    require("fs").readFileSync("./target/idl/give_away_app.json", "utf8")
  );

let accountPrivKey=[10,253,54,31,72,166,218,19,232,230,34,160,61,168,131,124,210,200,176,27,106,10,193,194,185,33,2,177,22,104,131,211,115,37,129,62,106,8,148,244,136,49,12,128,247,75,199,128,229,66,147,206,80,68,111,148,147,59,168,48,7,232,195,2].slice(0,32);
let User_Wallet = anchor.web3.Keypair.fromSeed(Uint8Array.from(accountPrivKey));
let ownerPrivKey=[39,160,120,239,225,24,95,19,92,144,94,226,150,216,128,201,145,143,222,236,8,183,212,19,29,153,5,127,51,187,117,47,213,34,250,84,112,151,98,64,219,21,18,214,100,71,241,80,54,111,100,12,183,247,204,39,35,122,164,131,136,178,202,159].slice(0,32);
let owner_wallet = anchor.web3.Keypair.fromSeed(Uint8Array.from(ownerPrivKey));
let winnerPrivKey=[228,59,41,142,207,58,71,38,226,0,228,204,77,99,159,38,104,247,9,212,30,176,66,55,232,234,84,127,54,129,183,192,248,188,109,32,38,236,128,172,4,135,64,100,149,91,90,194,112,22,184,9,160,145,54,42,161,97,101,29,117,102,62,255].slice(0,32);
let winner_wallet = anchor.web3.Keypair.fromSeed(Uint8Array.from(ownerPrivKey));
let transaction = new anchor.web3.Transaction();
let scobyPrivKey =[162,238,100,159,106,255,178,235,18,131,106,55,246,62,65,14,166,255,75,2,206,11,11,94,214,103,95,95,36,169,189,9,89,179,166,111,171,181,165,189,199,145,157,182,223,83,19,195,76,21,219,209,127,172,71,70,54,240,68,255,160,147,155,87].slice(0,32);
let scoby_wallet = anchor.web3.Keypair.fromSeed(Uint8Array.from(scobyPrivKey));
const con = new Connection("http://localhost:8899");
const program = new anchor.Program(idl, program_id);
let token_arr=[];
let token_arr2=[];
//const program = anchor.workspace.DevGiveAway as Program<DevGiveAway>;

it("Is initialized!", async () => {
  // Add your test here.
  let transaction = new anchor.web3.Transaction();
  let owner_wallet_airdrop= await con.requestAirdrop(owner_wallet.publicKey, 5000000000);
  await con.confirmTransaction(owner_wallet_airdrop);
  let user_wallet_airdrop= await con.requestAirdrop(User_Wallet.publicKey, 5000000000);
  await con.confirmTransaction(user_wallet_airdrop);
  let winner_wallet_airdrop= await con.requestAirdrop(winner_wallet.publicKey, 5000000000);
  await con.confirmTransaction(winner_wallet_airdrop);

  let scoby_wallet_airdrop= await con.requestAirdrop(scoby_wallet.publicKey, 5000000000);
  await con.confirmTransaction(scoby_wallet_airdrop);
  const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
    [owner_wallet.publicKey.toBuffer()],
    program_id
  );
  if(await con.getAccountInfo(user_pda_ac) == null)
  {
    
    transaction.add(await program.methods.initializeOwner(user_bump)
    .accounts({
      accountUser : user_pda_ac,
      owner : owner_wallet.publicKey,
      platform : scoby_wallet.publicKey,
      systemProgram : anchor.web3.SystemProgram.programId,
    }).signers([owner_wallet])
    .instruction())

   await sendAndConfirmTransaction(con,transaction,[owner_wallet]);

}

  let ownerpda= await program.account.accountUser.fetch(user_pda_ac);
  console.log("owner key on blockchain",ownerpda.ownerId.toString());
  console.log("owner key actual",owner_wallet.publicKey.toString());
  console.log("platform key on blockchain",ownerpda.platformId.toString());
  console.log("platform key actual",scoby_wallet.publicKey.toString());
  console.log("success");

  
});

it("create give away for solana transfer !", async () => {
  let transaction = new anchor.web3.Transaction();

  console.log("solana transfer from user to PDA started..");
  let msg_id="247";
  let no_of_sol=1;

  const [user_pda_sol, user_bump_sol] = 
  await anchor.web3.PublicKey.findProgramAddress(
    [User_Wallet.publicKey.toBuffer(),Buffer.from(msg_id)],
    program_id
  );

 let check_sol_pda = await con.getAccountInfo(user_pda_sol);
 if(check_sol_pda==null)
 {
  console.log("PDA for initialisation started");
  transaction.add(await program.methods.initializepdasol(user_bump_sol,msg_id)
  .accounts({
    userPdaAcct: user_pda_sol,
    sender: User_Wallet.publicKey,
    systemProgram: anchor.web3.SystemProgram.programId,
  }).signers([User_Wallet])
  .instruction())
 }

 transaction.add(await program.methods.sendsolpda(user_bump_sol,
  new anchor.BN(no_of_sol*LAMPORTS_PER_SOL),msg_id)
.accounts({
  userPdaAcct: user_pda_sol,
  sender: User_Wallet.publicKey,
  systemProgram: anchor.web3.SystemProgram.programId,
}).signers([User_Wallet])
.instruction())

await sendAndConfirmTransaction(con,transaction,[User_Wallet]);

console.log("solana transfer to PDA completed");
let sol_pda = await con.getAccountInfo(user_pda_sol);
console.log("solana in PDA =",sol_pda.lamports);
  
});

it("claim give away for solana transfer !", async () => {
  let transaction = new anchor.web3.Transaction();
      console.log("claim solana process started");
      let msg_id="247";
      const [user_pda_sol, user_bump_sol] = 
      await anchor.web3.PublicKey.findProgramAddress(
        [User_Wallet.publicKey.toBuffer(),Buffer.from(msg_id)],
        program_id
      );
  
      if(await con.getAccountInfo(user_pda_sol)==null)
      {
        console.log("pda account doesnt exist");
      }
  
      else
      {
      let pda_balance= await con.getAccountInfo(user_pda_sol);
      console.log("pda balance before claim",pda_balance.lamports);

      const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
        [owner_wallet.publicKey.toBuffer()],
        program_id
      );
  
      transaction.add(await program.methods.claimsol(user_bump_sol,msg_id)
      .accounts({
        userPdaAcct: user_pda_sol,
        sender: User_Wallet.publicKey,
        winner : winner_wallet.publicKey,
        platform : scoby_wallet.publicKey,
        owner : user_pda_ac,
        systemProgram: anchor.web3.SystemProgram.programId,
      }).signers([])
      .instruction())
      
      console.log("claim call");
      await sendAndConfirmTransaction(con,transaction,[scoby_wallet]);
  
      console.log("solana claim process completed");
  
      let pda_balance_after= await con.getAccountInfo(user_pda_sol);
      console.log("pda balance after claim",pda_balance_after.lamports);
  
    }
  
  
    });

it("create give away for Token transfer !", async () => {
      let transaction = new anchor.web3.Transaction();
      console.log("Token transfer from user to PDA started..");
      let msg_id="247";
      let mintA = await createMint(con, User_Wallet, User_Wallet.publicKey, null, 0);
      let myToken_acctA = await getOrCreateAssociatedTokenAccount(con,
        User_Wallet,
        mintA,
        User_Wallet.publicKey);
    
        await mintTo(con,User_Wallet,mintA,
          myToken_acctA.address, User_Wallet.publicKey, 5);
          let token_acct=myToken_acctA;
          let mint=mintA;
          let amount=1;
          token_arr.push(token_acct,mint,amount)
    
          // state PDA for token
          const [user_pda_state, bump_state] = await anchor.web3.PublicKey.findProgramAddress(
            [User_Wallet.publicKey.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
            program_id
          );
    
          console.log("state acount",user_pda_state.toString());
    
          if(await con.getAccountInfo(user_pda_state)==null)
          {
    
            transaction.add(await program.methods.initializestatepda(bump_state,
              msg_id)
            .accounts({
              statepda: user_pda_state,
              owner: User_Wallet.publicKey,
              depositTokenAccount: token_acct.address,
              systemProgram: anchor.web3.SystemProgram.programId,
            }).signers([User_Wallet])
            .instruction())
          }
    
    
          /// token PDA 
          const [usertokenpda, bump_token] = await anchor.web3.PublicKey.findProgramAddress(
            [User_Wallet.publicKey.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id)],
            program_id
          );
    
          console.log("token account",usertokenpda.toString());
    
          if(await con.getAccountInfo(usertokenpda)==null)
          {
    
            transaction.add(await program.methods.initialisetokenpda(bump_token,
              msg_id)
            .accounts({
              tokenpda: usertokenpda,
              statepda: user_pda_state,
              mint :    mint,
              owner : User_Wallet.publicKey,
              depositTokenAccount : token_acct.address,
              systemProgram: anchor.web3.SystemProgram.programId,
              rent : SYSVAR_RENT_PUBKEY,
              tokenProgram : TOKEN_PROGRAM_ID
            }).signers([User_Wallet])
            .instruction())
          }
    
          /// call for token transfer from user to PDA token Account
          transaction.add(await program.methods.sendtokenpda(bump_state,bump_token,
            new anchor.BN(amount),
            msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            mint :    mint,
            owner : User_Wallet.publicKey,
            depositTokenAccount : token_acct.address,
            systemProgram: anchor.web3.SystemProgram.programId,
            rent : SYSVAR_RENT_PUBKEY,
            tokenProgram : TOKEN_PROGRAM_ID
          }).signers([User_Wallet])
          .instruction())
    
          await sendAndConfirmTransaction(con,transaction,[User_Wallet]);
          console.log("token transfer completed");    
          
      
      
    });

it("claim give away for Token transfer !", async () => {
      let transaction = new anchor.web3.Transaction();
      let msg_id="247";
      
        let tokenaccount=token_arr[0];
        let mint=token_arr[1];
        let amount=token_arr[2];
        
    
        const [user_pda_state, bump1] = await anchor.web3.PublicKey.findProgramAddress(
          [User_Wallet.publicKey.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
          program_id
        );
    
        const [usertokenpda, bump2] = await anchor.web3.PublicKey.findProgramAddress(
          [User_Wallet.publicKey.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id)],
          program_id
        );

        const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
          [owner_wallet.publicKey.toBuffer()],
          program_id
        );

        if(await con.getAccountInfo(user_pda_ac) == null)
        {
          console.log("pda does not exist");
        }
    
        let wallet_to_deposit_to = await getOrCreateAssociatedTokenAccount(
          con,
          winner_wallet,
          mint,
          winner_wallet.publicKey,
          );
    
          transaction.add(await program.methods.sendtokenwinner(bump1,bump2,
            new anchor.BN(amount),msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            walletToDepositTo: wallet_to_deposit_to.address,
            sender: User_Wallet.publicKey,
            depositTokenAccount : tokenaccount.address,
            reciever: winner_wallet.publicKey,
            platform : scoby_wallet.publicKey,
            owner : user_pda_ac,
            //mint:      mint,       
            systemProgram: anchor.web3.SystemProgram.programId,
            rent: SYSVAR_RENT_PUBKEY,
            associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
            tokenProgram:TOKEN_PROGRAM_ID
          }).signers([scoby_wallet])
          .instruction())
    
          await sendAndConfirmTransaction(con,transaction,[scoby_wallet]);
    
    });

it("create give away for solana transfer !", async () => {


      let transaction = new anchor.web3.Transaction();
    
      console.log("solana transfer from user to PDA started..");
      let msg_id="247";
      let no_of_sol=1;
    
      const [user_pda_sol, user_bump_sol] = 
      await anchor.web3.PublicKey.findProgramAddress(
        [User_Wallet.publicKey.toBuffer(),Buffer.from(msg_id)],
        program_id
      );
    
     let check_sol_pda = await con.getAccountInfo(user_pda_sol);
     if(check_sol_pda==null)
     {
      console.log("PDA for initialisation started");
      transaction.add(await program.methods.initializepdasol(user_bump_sol,msg_id)
      .accounts({
        userPdaAcct: user_pda_sol,
        sender: User_Wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId,
      }).signers([User_Wallet])
      .instruction())
     }
    
     transaction.add(await program.methods.sendsolpda(user_bump_sol,
      new anchor.BN(no_of_sol*LAMPORTS_PER_SOL),msg_id)
    .accounts({
      userPdaAcct: user_pda_sol,
      sender: User_Wallet.publicKey,
      systemProgram: anchor.web3.SystemProgram.programId,
    }).signers([User_Wallet])
    .instruction())
    
    await sendAndConfirmTransaction(con,transaction,[User_Wallet]);
    
    console.log("solana transfer to PDA completed");
    let sol_pda = await con.getAccountInfo(user_pda_sol);
    console.log("solana in PDA =",sol_pda.lamports);
      
    });

it("cancel give away for Sol transfer !", async () => {

      let transaction = new anchor.web3.Transaction();
      let msg_id="247"
    
      const [user_pda_sol, user_bump_sol] = 
      await anchor.web3.PublicKey.findProgramAddress(
        [User_Wallet.publicKey.toBuffer(),Buffer.from(msg_id)],
        program_id
      );
    
      console.log("rpc call for cancel sol  started..");

      const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
        [owner_wallet.publicKey.toBuffer()],
        program_id
      );
    
      transaction.add(await program.methods.cancelSol(user_bump_sol,msg_id)
           .accounts({
             userPdaAcct: user_pda_sol,
             sender: User_Wallet.publicKey,
             owner: owner_wallet.publicKey,
             ownerpda : user_pda_ac,
             systemProgram: anchor.web3.SystemProgram.programId,
           }).signers([owner_wallet])
           .instruction())
    
     console.log("cancel call for Sol completed");
    
     await sendAndConfirmTransaction(con,transaction,[owner_wallet]);
    
     
     let sol_pda = await con.getAccountInfo(user_pda_sol);
      console.log("solana in PDA after cancel =",sol_pda.lamports);
    
    });

it("create give away for Token transfer !", async () => {

      let transaction = new anchor.web3.Transaction();
      console.log("Token transfer from user to PDA started..");
      let msg_id="247";
      let mintA = await createMint(con, User_Wallet, User_Wallet.publicKey, null, 0);
      let myToken_acctA = await getOrCreateAssociatedTokenAccount(con,
        User_Wallet,
        mintA,
        User_Wallet.publicKey);
    
        await mintTo(con,User_Wallet,mintA,
          myToken_acctA.address, User_Wallet.publicKey, 5);
          let token_acct=myToken_acctA;
          let mint=mintA;
          let amount=1;
          token_arr2.push(token_acct,mint,amount)
    
          // state PDA for token
          const [user_pda_state, bump_state] = await anchor.web3.PublicKey.findProgramAddress(
            [User_Wallet.publicKey.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
            program_id
          );
    
          console.log("state acount",user_pda_state.toString());
    
          if(await con.getAccountInfo(user_pda_state)==null)
          {
    
            transaction.add(await program.methods.initializestatepda(bump_state,
              msg_id)
            .accounts({
              statepda: user_pda_state,
              owner: User_Wallet.publicKey,
              depositTokenAccount: token_acct.address,
              systemProgram: anchor.web3.SystemProgram.programId,
            }).signers([User_Wallet])
            .instruction())
          }
    
    
          /// token PDA 
          const [usertokenpda, bump_token] = await anchor.web3.PublicKey.findProgramAddress(
            [User_Wallet.publicKey.toBuffer(),token_acct.address.toBuffer(),Buffer.from(msg_id)],
            program_id
          );
    
          console.log("token account",usertokenpda.toString());
    
          if(await con.getAccountInfo(usertokenpda)==null)
          {
    
            transaction.add(await program.methods.initialisetokenpda(bump_token,
              msg_id)
            .accounts({
              tokenpda: usertokenpda,
              statepda: user_pda_state,
              mint :    mint,
              owner : User_Wallet.publicKey,
              depositTokenAccount : token_acct.address,
              systemProgram: anchor.web3.SystemProgram.programId,
              rent : SYSVAR_RENT_PUBKEY,
              tokenProgram : TOKEN_PROGRAM_ID
            }).signers([User_Wallet])
            .instruction())
          }
    
          /// call for token transfer from user to PDA token Account
          transaction.add(await program.methods.sendtokenpda(bump_state,bump_token,
            new anchor.BN(amount),
            msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            mint :    mint,
            owner : User_Wallet.publicKey,
            depositTokenAccount : token_acct.address,
            systemProgram: anchor.web3.SystemProgram.programId,
            rent : SYSVAR_RENT_PUBKEY,
            tokenProgram : TOKEN_PROGRAM_ID
          }).signers([User_Wallet])
          .instruction())
    
          await sendAndConfirmTransaction(con,transaction,[User_Wallet]);
          console.log("token transfer completed");    
          
      
      
    });

it("cancel give away for Token transfer !", async () => {

      let transaction = new anchor.web3.Transaction();
      let msg_id="247";
      
        let tokenaccount=token_arr2[0];
        let mint=token_arr2[1];
        let amount=token_arr2[2];
        
    
        const [user_pda_state, bump1] = await anchor.web3.PublicKey.findProgramAddress(
          [User_Wallet.publicKey.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id),Buffer.from("state")],
          program_id
        );
    
        const [usertokenpda, bump2] = await anchor.web3.PublicKey.findProgramAddress(
          [User_Wallet.publicKey.toBuffer(),tokenaccount.address.toBuffer(),Buffer.from(msg_id)],
          program_id
        );

        const [user_pda_ac, user_bump] = await PublicKey.findProgramAddress(
          [owner_wallet.publicKey.toBuffer()],
          program_id
        );
    
        let wallet_to_deposit_to = await getOrCreateAssociatedTokenAccount(
          con,
          User_Wallet,
          mint,
          User_Wallet.publicKey,
          );
    
          transaction.add(await program.methods.canceltoken(bump1,bump2,
            new anchor.BN(amount),msg_id)
          .accounts({
            tokenpda: usertokenpda,
            statepda: user_pda_state,
            walletToDepositTo: wallet_to_deposit_to.address,
            sender: User_Wallet.publicKey,
            owner: owner_wallet.publicKey,
            depositTokenAccount : tokenaccount.address,
            ownerpda : user_pda_ac,
            //mint:      mint,       
            systemProgram: anchor.web3.SystemProgram.programId,
            rent: SYSVAR_RENT_PUBKEY,
            associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
            tokenProgram:TOKEN_PROGRAM_ID
          }).signers([owner_wallet])
          .instruction())
    
          await sendAndConfirmTransaction(con,transaction,[owner_wallet]);
    
    });
    

});



